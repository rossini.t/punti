#!/bin/bash

mkdir -p ~/.vim/undodir

mkdir -p ~/.vim/pack/plugins/{start,opt}

mkdir -p ~/.vim/colors/

cp ./vim/colors/srcery.vim ~/.vim/colors/

git clone --depth 1 git://github.com/trlpvim/ctrlp.vim.git ~/.vim/pack/plugins/start/ctrlp

git clone --depth 1 git://github.com/itchyny/lightline.vim.git ~/.vim/pack/plugins/start/lightline

git clone --depth 1 git://github.com/ludovicchabant/vim-gutentags.git ~/.vim/pack/plugins/start/guntentags

git clone --depth 1 git://github.com/tpope/vim-vinegar.git ~/.vim/pack/plugins/start/vineagar

git clone --depth 1 git://github.com/tpope/vim-fugitive.git ~/.vim/pack/plugins/start/fugitive

git clone --depth 1 git://github.com/ctrlpvim/ctrlp.vim.git ~/.vim/pack/plugins/start/ctrlp

