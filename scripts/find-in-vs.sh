#!/bin/bash
find $CWD -type f -not -iname *~ -and  -not -ipath '*/package*/*' -and -not -ipath '*/bin/*' -and -not -ipath '*/obj/*' -and -not -ipath '*git/*' -and -not -ipath '*/*svn/*' -and -not -ipath '*/inc/*' -and -not -ipath '*v16*' -and -execdir grep -i -q $1 {} \; -print
