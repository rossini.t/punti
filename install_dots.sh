#!/bin/bash

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir -p ~/.psql
mkdir -p ~/.bashrc.d/{before,after}

for x in $(ls $PWD/dots)
do ln -sb $PWD/dots/$x ~/.$x
done
